package csta

import (
	"fmt"
	"log"
	"sync"
)

// DefaultMonitorsLength указывает количество мониторов в очереди, под которые сразу выделяется
// память.
var DefaultMonitorsLength = 100

type CallEvent uint8

const (
	CallEventNone CallEvent = iota // инициализация процесса дозвона
	CallEventMake
	CallEventClear       // остановка звонка
	CallEventEstablished // звонок состоялся
	CallEventDelivered
	CallEventDiverted
	CallEventConnectionError // ошибка соединения с сервером
)

// Monitor описывает информацию о звонке и его статусы.
type Monitor struct {
	CallID   string         // номер процесса
	DeviceID string         // внутренний номер, с которого идет звонок
	To       string         // номер, на который идет звонок
	Events   chan CallEvent // канал для отдачи событий
	sync.RWMutex
}

// setCallID устанавливает новый идентификатор звонка.
func (m *Monitor) setCallID(callID string) {
	m.Lock()
	m.CallID = callID
	m.Unlock()
}

// MonitorList описывает список мониториемых звонков.
type MonitorList struct {
	list   map[string]*Monitor // список мониториемых звонков
	logger *log.Logger         // для вывода лога
	mu     sync.RWMutex        // блокировщик доступа
}

// log выводит информацию в лог
func (ml *MonitorList) log(v ...interface{}) {
	if ml.logger != nil {
		ml.logger.Output(3, fmt.Sprintln(v...))
	}
}

// add добавляет в список информацию о новом звонке и возвращает канал, по которому будет
// происходить уведомление о состоянии звонка.
func (ml *MonitorList) add(deviceID, to string) <-chan CallEvent {
	ml.log("add from:", deviceID, "to:", to)
	// проверяем, что это устройство еще не звонит
	ml.mu.RLock()
	_, ok := ml.list[deviceID]
	ml.mu.RUnlock()
	if ok {
		ml.log("ignore add from:", deviceID, "- already exist")
		return nil // возвращаем пустой канал, если это устройство уже в списке звонков
	}
	events := make(chan CallEvent, 10) // инициализируем канал со статусами звонка
	// добавляем описание звонка в список
	ml.mu.Lock()
	ml.list[deviceID] = &Monitor{DeviceID: deviceID, To: to, Events: events}
	ml.mu.Unlock()
	return events
}

// send отправляет уведомление о статусе на устройство с указанным идентификатором. В ответ
// возвращает true, если устройство с таким идентификатором зарегистрировано в списке.
func (ml *MonitorList) set(deviceID, callID string, status CallEvent) bool {
	ml.log("set from:", deviceID, "callID:", callID, "staus:", status)
	ml.mu.RLock()
	defer ml.mu.RUnlock()
	var monitor *Monitor
	// получаем информацию о мониторе
	if deviceID != "" {
		monitor = ml.list[deviceID]
	} else {
		// альтернативный способ по идентификатору вызова
		for _, item := range ml.list {
			if item.CallID == callID {
				monitor = item
				break
			}
		}
	}
	if monitor == nil {
		ml.log("ignore set from:", deviceID, "- not exist")
		return false // если такой монитор не найден, то возвращаемся
	}
	// изменяем идентификатор процесса, если он изменился
	if monitor.CallID != callID {
		monitor.Lock()
		monitor.CallID = callID
		monitor.Unlock()
	}
	// отсылаем сообщение об изменении статуса
	if status != CallEventNone {
		monitor.Events <- status
	}
	return true
}

// connectionError рассылает сообщение об ошибке всем мониторам, которые сейчас в очереди.
func (ml *MonitorList) connectionError() {
	if len(ml.list) == 0 {
		return // ничего не делаем, если в списке нет мониторов
	}
	ml.log("connectionError - clear all", len(ml.list), "items")
	ml.mu.Lock()
	// рассылаем всем сообщение
	for _, monitor := range ml.list {
		monitor.Lock()
		monitor.Events <- CallEventConnectionError
		close(monitor.Events)
		monitor.Unlock()
	}
	// очищаем весь список мониторов
	ml.list = make(map[string]*Monitor, DefaultMonitorsLength)
	ml.mu.Unlock()
}
