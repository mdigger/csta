package csta

import (
	"reflect"
	"time"
)

// Интервал для автоматического обновления адресной книги.
var AddresBookUpdateDuration = time.Hour

// ServerAddressbook описывает пользовательское соединение с поддержкой адресной книги и списка
// избранных контактов.
type ServerAddressbook struct {
	conn         *Conn // соединение с сервером
	*AddressBook       // адресная книга
	*Favourites        // список избранных
}

// NewServerAddressbook возвращает описание нового инициализированного пользовательского соединения
// с сервером, в котором встроена поддержка работы с адресной книгой и избранными контактами.
//
// Пользовательское соединение добавляет обработку следующих событий: ablist, contact,
// removeContact и presence.
func NewServerAddressbook(conn *Conn, addressBook *AddressBook, favourites *Favourites) *ServerAddressbook {
	// регистрируем дополнительные события, которые мы хотим обрабатывать
	conn.Register(EventsDataMap{
		"ablist":        reflect.TypeOf(ABList{}),
		"contact":       reflect.TypeOf(Contact{}),
		"removeContact": reflect.TypeOf(RemoveContact{}),
		"presence":      reflect.TypeOf(Presence{}),
	})
	if addressBook == nil {
		addressBook = new(AddressBook)
	}
	if favourites == nil {
		favourites = new(Favourites)
	}
	// возвращаем описание пользовательского соединения
	return &ServerAddressbook{
		conn:        conn,
		AddressBook: addressBook,
		Favourites:  favourites,
	}
}

// Connect устанавливает соединение с сервером и начинает получать разобранные сообщения от него.
//
// Автоматически разбирает получаемую адресную книгу и отслеживает изменения в списке избранных
// контактов. Интервал, через который осуществляется автоматическое обновление адресной книги,
// задается в AddresBookUpdateDuration. Если вы хотите его изменить, то это необходимо сделать до
// вызова данного метода.
//
// В случае завершения соединения, канал с событиями автоматически закрывается.
func (sa *ServerAddressbook) Connect(output chan<- interface{}) error {
	var events = make(chan interface{})             // создаем канал для приема событий
	if err := sa.conn.Connect(events); err != nil { // устанавливаем соединение
		return err
	}
	// запускаем периодическое обновление адресной книги
	var addressbookTicker = time.NewTicker(AddresBookUpdateDuration)
	go func() {
		for range addressbookTicker.C {
			sa.RequestAddressBook()
		}
	}()
	// запускаем обработчик событий от сервера
	go func() {
		var (
			connErr     error              // последняя ошибка
			addressBook map[string]ABEntry // временная адресная книга
		)
	eventsLoop: // обрабатываем все получаемые сообщения
		for event := range events {
			// определяем тип данных и события
			switch data := event.(type) {
			case *ABList: // список пользователей в адресной книге
				if data.Index == 0 { // инициализируем адресную книгу при получении первой порции
					addressBook = make(map[string]ABEntry, data.Size)
				}
				// заполняем адресную книгу полученными данными
				for _, item := range data.ABEntries {
					addressBook[item.Ext] = item
				}
				// проверяем, что адресная книга получена еще не вся
				if total := data.Index + len(data.ABEntries); total < data.Size {
					// запрашиваем следующую порцию адресной книги
					if connErr = sa.conn.GetAddressBook(total); connErr != nil {
						break eventsLoop
					}
					// ждем получение остальной части адресной книги,
					// а события о неполной адресной книге игнорируем и не передаем в канал
					continue
				}
				// вся адресная книга получена - делаем ее глобально доступной
				sa.AddressBook.mu.Lock()
				sa.AddressBook.list = addressBook
				sa.AddressBook.mu.Unlock()
				// инициируем получение информации об избранных контактах
				if connErr = sa.conn.GetBuddyList(); connErr != nil {
					break eventsLoop
				}
				continue // не передаем событие в канал и при получении всей адресной книги
			case *Contact: // информация об избранном контакте
				sa.Favourites.add(data)
			case *RemoveContact: // информация об удалении контакта из избранных
				sa.Favourites.remove(data.JID)
			case *Presence: // информация об изменении статуса избранного контакта
				if data.JID == "0" { // информация о самом себе
					// делаем запрос на получение адресной книги
					if connErr = sa.RequestAddressBook(); connErr != nil {
						break eventsLoop // прерываем соединение
					}
					break // дальне не обрабатываем, а сразу отдаем к канал
				}
				// заносим изменения в список избранных
				if contact := sa.GetFavourites(data.JID); contact != nil {
					contact.Status = data.Status
					contact.Note = data.Note
					sa.Favourites.add(contact)
				}
			case error: // ошибка чтения или закрытое соединение
				connErr = data
				break eventsLoop // прерываем обработку событий
			default: // все остальные события
				continue // игнорируем их и даже не отдаем в канал для дальнейшей обработки
			}
			// отправляем все события, которые сюда дошли, дальше
			if output != nil {
				output <- event
			}
		}

		// останавливаем автоматическое обновление адресной книги
		if addressbookTicker != nil {
			addressbookTicker.Stop()
		}
		// все события обработаны - входящий канал закрыт
		if output != nil {
			if connErr != nil {
				output <- connErr // отправляем ошибку
			}
			close(output) // закрываем канал по окончании обработки событий
		}
	}()

	return nil
}

// RequestAddressBook инициализирует запрос получения адресной книги с сервера. Обычно вызов
// этого метода напрямую не требуется, т.к. адресная книга запрашивается автоматически сразу
// после авторизации и в дальнейшем так же автоматически обновляется с указанным интервалом.
func (sa *ServerAddressbook) RequestAddressBook() error {
	return sa.conn.GetAddressBook(0)
}

// AddToFavourites инициализирует запрос на добавление пользователя с указанным уникальным
// идентификатором в список избранных.
func (sa *ServerAddressbook) AddToFavourites(JID string) error {
	return sa.conn.AddToBuddyList(JID)
}

// RemoveFromFavourites инициализирует запрос на удаление пользователя с указанным уникальным
// идентификатором из списка избранных.
func (sa *ServerAddressbook) RemoveFromFavourites(JID string) error {
	return sa.conn.RemoveFromBuddyList(JID)
}
