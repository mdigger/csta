package csta

import "sync"

// Favourites описывает список избранных контактов.
type Favourites struct {
	list map[string]Contact // список избранных контактов по их уникальному идентификатору
	mu   sync.RWMutex
}

// add добавляет контакт в список избранных.
func (f *Favourites) add(contact *Contact) {
	f.mu.Lock()
	if f.list == nil {
		f.list = make(map[string]Contact)
	}
	f.list[contact.Ext] = *contact
	f.mu.Unlock()
}

// remove удаляет контакт с указанным идентификатором из списка избранных.
func (f *Favourites) remove(jid string) {
	if f.list != nil {
		f.mu.Lock()
		delete(f.list, jid)
		f.mu.Unlock()
	}
}

// GetFavourites возвращает информацию об избранном контакте по его внутреннему уникальному
// идентификатору. Если пользователя с таким идентификатором нет в избранных контактах, то
// возвращается nil.
func (f *Favourites) GetFavourites(jid string) *Contact {
	f.mu.RLock()
	defer f.mu.RUnlock()
	if contact, ok := f.list[jid]; ok {
		return &contact
	}
	return nil
}
