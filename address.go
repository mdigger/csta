package csta

import "time"

// Предопределенные по умолчанию значения для установки соединения.
var (
	DefaultConnectionTimeout = time.Second * 5 // время ожидания соединения по умолчанию
	DefaultNetwork           = "tcp"           // тип соединения
)

// Address описывает информацию для подключения к MX-серверу.
//
// Для использования защищенного подключения необходимо взвести флаг IsTLS.
//
// Если на сервере используются самоподписанные сертификаты, то необходимо взвести флаг
// IsInsecureSkipVerify, который отключает проверку валидности сертификата.
//
// Если ConnectionTimeout не установлен, то будет использоваться значение по умолчанию -
// DefaultConnectionTimeout.
//
// В качестве Network можно использовать "tcp", "udp" и прочее. По умолчанию используется DefaultNetwork.
type Address struct {
	Server               string        `json:"server"`               // адрес сервера и порт
	IsTLS                bool          `json:"TLS,omitempty"`        // флаг шифрованного соединения
	IsInsecureSkipVerify bool          `json:"skipVerify,omitempty"` // не использовать проверку сертификата
	ConnectionTimeout    time.Duration `json:"timeout,omitempty"`    // время ожидания соединения
	Network              string        `json:"network,omitempty"`    // тип соединения
}

// getConnectionTimeout возвращает время ожидания соединения. Если оно не установлено, то
// возвращается DefaultConnectionTimeout.
func (a Address) getConnectionTimeout() time.Duration {
	if a.ConnectionTimeout > 0 {
		return a.ConnectionTimeout
	}
	return DefaultConnectionTimeout
}

// getDeadline возвращает абсолютное время Deadline для соединения с учетом текущего времени и
// установленного ConnectionTimeout для данного соединения.
func (a Address) getDeadline() time.Time {
	return time.Now().Add(a.getConnectionTimeout())
}

// getNetwork возвращает строку с типом соединения. По умолчанию - DefaultNetwork.
func (a Address) getNetwork() string {
	if a.Network != "" {
		return a.Network
	}
	return DefaultNetwork
}
