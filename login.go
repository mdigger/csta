package csta

import (
	"crypto/sha1"
	"encoding/base64"
)

// Предопределенные значения платформы и версии, используемые при логине.
var (
	DefaultPlatform = "iPhone" // название платформы, используемой для логина по умолчанию.
	DefaultVersion  = "N/A"    // версия платформы
)

// Login описывает информацию для авторизации на MX-сервере.
type Login struct {
	Name     string `json:"login"`              // логин пользователя
	Password string `json:"password"`           // пароль в открытом виде
	AsClear  bool   `json:"clear,omitempty"`    // пароль передавать в открытом виде
	Type     string `json:"type,omitempty"`     // тип учетной записи: User, Server, Group
	Platform string `json:"platform,omitempty"` // название платформы
	Version  string `json:"version,omitempty"`  // версия программного обеспечения
}

// getType возвращает строку с типом авторизационной информации. Значение по умолчанию - "User".
func (l Login) getType() string {
	if l.Type == "" {
		return "User"
	}
	return l.Type
}

// getPlatform возвращает название платформы. Значение по умолчанию - DefaultPlatform.
func (l Login) getPlatform() string {
	if l.Platform != "" {
		return l.Platform
	}
	return DefaultPlatform
}

// getVersion возвращает версию платформы. Значение по умолчанию - DefaultVersion.
func (l Login) getVersion() string {
	if l.Version != "" {
		return l.Version
	}
	return DefaultVersion
}

// HashPassword возвращает строку с хешированным паролем.
func (l Login) hashPassword() string {
	var pwdHash = sha1.Sum([]byte(l.Password))                  // хешируем пароль
	return base64.StdEncoding.EncodeToString(pwdHash[:]) + "\n" // переводим в base64
}
