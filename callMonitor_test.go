package csta

import (
	"log"
	"os"
	"testing"
	"time"
)

func TestMonitor(t *testing.T) {
	conn := NewCallMonitor(NewConn(address, &Login{
		Name:     "orchestration",
		Password: "xopen123",
		Type:     "System",
	}))
	conn.monitors.logger = log.New(os.Stderr, "", log.Lshortfile)
	if err := conn.Connect(); err != nil {
		t.Fatal(err)
	}
	time.Sleep(time.Second * 5)
	log.Println("MAKE CALL")
	events, err := conn.MakeCall("179", "106")
	if err != nil {
		t.Fatal(err)
	}
	for event := range events {
		log.Println("event:", event)
	}
	log.Println("END")

}
