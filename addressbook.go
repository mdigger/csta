package csta

import "sync"

// AddressBook описывает серверную адресную книгу MX-сервера.
type AddressBook struct {
	list map[string]ABEntry
	mu   sync.RWMutex
}

// GetByExt возвращает информацию о пользователе по его внутреннему номеру.
func (ab *AddressBook) GetByExt(ext string) *ABEntry {
	ab.mu.RLock()
	defer ab.mu.RUnlock()
	if ab.list == nil {
		return nil
	}
	if item, ok := ab.list[ext]; ok {
		return &item
	}
	return nil
}

// GetByJID возвращает информацию о пользователе по его уникальному идентификатору.
func (ab *AddressBook) GetByJID(jid string) *ABEntry {
	ab.mu.RLock()
	defer ab.mu.RUnlock()
	if ab.list == nil {
		return nil
	}
	for _, item := range ab.list {
		if item.JID == jid {
			return &item
		}
	}
	return nil

}
