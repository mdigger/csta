package csta

import (
	"encoding/xml"
	"fmt"
	"testing"
)

func TestCSTAError(t *testing.T) {
	var (
		events = []string{
			`<CSTAErrorCode><privateErrorCode>User Unreachable</privateErrorCode></CSTAErrorCode>`,
			`<CSTAErrorCode><operation>Unbind</operation></CSTAErrorCode>`,
		}
		data ErrorCode
	)
	for _, event := range events {
		if err := xml.Unmarshal([]byte(event), &data); err != nil {
			t.Fatal(err)
		}
		fmt.Printf("%#v\n", data)
	}
}
