package csta

import (
	"bytes"
	"crypto/tls"
	"encoding/binary"
	"encoding/xml"
	"fmt"
	"io"
	"log"
	"net"
	"reflect"
	"sync/atomic"
	"time"
)

// Интервал для отправки keep-alive сообщений.
var DefaultKeepAliveDuration = time.Second * 30

// Conn описывает соединение с MX-сервером.
type Conn struct {
	addr    *Address // адрес MX-сервера
	login   *Login   // информация для авторизации
	conn    net.Conn // соединение с MX
	counter uint32   // счетчик отправленных команд
	Events           // поддерживаемые события и описания их данных
}

// NewConn возвращает описание нового соединения с MX-сервером.
//
// По умолчанию соединение поддерживает обработку следующих событий: CSTAErrorCode, loginResponce
// и loginFailed.
func NewConn(address *Address, login *Login) *Conn {
	return &Conn{
		addr:  address,
		login: login,
		Events: Events{
			list: map[string]reflect.Type{
				"CSTAErrorCode": reflect.TypeOf(ErrorCode{}),
				"loginResponce": reflect.TypeOf(LoginResponce{}),
				"loginFailed":   reflect.TypeOf(LoginResponce{}),
			},
		},
	}
}

// Close закрывает соединение с сервером.
func (c *Conn) Close() error {
	if c.conn != nil {
		return c.conn.Close()
	}
	return nil
}

// Send отправляет команду на сервер. Строка с командой должна соответствовать формату XML, т.к.
// внутри метода отправки команды такой проверки не происходит. XML-заголовок добавляется к команде
// автоматически, поэтому его не нужно указывать отдельно.
func (c *Conn) Send(cmd string) (err error) {
	if c.conn == nil {
		return io.EOF // возвращаем, что канал закрыт
	}
	log.Println("<-", cmd)
	// TODO: добавить пулл буферов
	var buf bytes.Buffer // инициализируем буфер для формирования команды
	// записываем два нулевых символа в качестве разделителя
	if _, err = buf.Write([]byte{0, 0}); err != nil {
		return
	}
	// длина сообщения с учетом длинны самих данных, XML-заголовка и счетчика
	length := uint16(len(cmd) + len(xml.Header) + 8)
	if err = binary.Write(&buf, binary.BigEndian, length); err != nil {
		return
	}
	// увеличиваем счетчик...
	counter := atomic.AddUint32(&c.counter, 1)
	if counter > 9999 { // под счетчик отведено только 4 цифры
		atomic.StoreUint32(&c.counter, 0) // сбрасываем счетчик
		counter = 0
	}
	// ...и добавляем его в сообщение
	if _, err = fmt.Fprintf(&buf, "%04d", counter); err != nil {
		return
	}
	// добавляем XML-заголовок
	if _, err = buf.WriteString(xml.Header); err != nil {
		return
	}
	// добавляем непосредственно текст команды
	if _, err = buf.WriteString(cmd); err != nil {
		return
	}
	// отправляем команду на сервер
	_, err = buf.WriteTo(c.conn)
	return
}

// Connect устанавливает соединение с сервером и выполняет логин, если он определен.
// Если в качестве параметра передан открытый канал, то в него будут поступать все определенные
// для обработки события в уже разобранном виде. Для регистрации новых событий воспользуйтесь
// методом Register(EventsDataMap).
//
// В процессе работы поддерживается автоматическая отправка keep-alive сообщений. Интервал для
// отправки таких сообщений задается в DefaultKeepAliveDuration. Если вы хотите изменить этот
// интервал, то это необходимо сделать до вызова данного метода.
//
// Если при инициализации соединения в него была передана информация о регистрации (login), то
// сразу после установки соединения будет автоматически произведена операция входа.
//
// В случае завершения соединения, канал с событиями автоматически закрывается.
func (c *Conn) Connect(events chan<- interface{}) (err error) {
	// установщик соединения
	var dialer = net.Dialer{
		Timeout:   c.addr.getConnectionTimeout(),
		KeepAlive: DefaultKeepAliveDuration,
	}
	var network = c.addr.getNetwork() // получаем тип соединения
	if c.addr.IsTLS {                 // устанавливаем защищенное соединение
		// не проверяем валидность сертификатов если задано в настройках
		var config = tls.Config{InsecureSkipVerify: c.addr.IsInsecureSkipVerify}
		c.conn, err = tls.DialWithDialer(&dialer, network, c.addr.Server, &config)
	} else { // устанавливаем незащищенное соединение
		c.conn, err = dialer.Dial(network, c.addr.Server)
	}
	if err != nil {
		return // в случае ошибки возвращаем ее
	}

	// таймер для отсылки keepAlive-сообщений
	var keepAliveTicker = time.NewTicker(DefaultKeepAliveDuration)
	// запускаем периодическую отправку keepAlive-сообщений
	go func() {
		for range keepAliveTicker.C {
			c.Send("<keepalive />") // отсылаем команду, ошибки игнорируем
		}

	}()

	// запускаем чтение ответов сервера
	go func() {
		var (
			header  = make([]byte, 8) // заголовок ответа
			connErr error             // последняя ошибка чтения ответов от сервера
		)
	readingLoop:
		for { // бесконечный цикл чтения всех сообщений из потока
			// читаем заголовок ответа
			if _, connErr = io.ReadFull(c.conn, header); connErr != nil {
				break // прерываем цикл чтения ответов
			}
			// получаем длину сообщения, выделяем под него память и читаем само сообщение
			var data = make([]byte, binary.BigEndian.Uint16(header[2:4])-8)
			if _, connErr = io.ReadFull(c.conn, data); connErr != nil {
				break // прерываем цикл чтения ответов
			}
			log.Println("->", string(data))
			// // инициализируем XML-декодер, получаем имя события и данные
			var xmlDecoder = xml.NewDecoder(bytes.NewReader(data))
			// разбираем полученные данные поэлементно
			for {
				var token, err = xmlDecoder.Token() // читаем название XML-элемента
				if err == io.EOF {
					break // разобрали все из текущих полученных данных
				}
				if err != nil {
					log.Println("Token Error:", err)
					continue // игнорируем сообщения с неверным XML - читаем следующий токен
				}
				// находим начальный элемент XML, а все остальное пропускаем
				var startToken, ok = token.(xml.StartElement)
				if !ok {
					continue // если это не корневой XML-элемент, то переходим к следующему
				}
				var eventName = startToken.Name.Local // получаем название события
				// получаем ссылку на соответствующую событию структуру данных для разбора
				var eventPointer = c.Events.getEventData(eventName)
				if eventPointer == nil { // тип события не определен для обработки
					log.Println("Ignore:", eventName)
					// пропускаем неизвестный элемент целиком
					if err := xmlDecoder.Skip(); err != nil {
						break // в случае ошибки прерываем чтение текущего блока
					}
					continue // пропускаем необрабатываемые события
				}
				// разбираем сами данные, вернувшиеся в описании события
				if err := xmlDecoder.DecodeElement(eventPointer, &startToken); err != nil {
					log.Println("Error decode element:", err)
					continue // игнорируем элементы, которые не смогли разобрать
				}
				// обрабатываем разобранные сообщения по типам
				switch data := eventPointer.(type) {
				case *ErrorCode: // ошибка CSTA
					connErr = fmt.Errorf("CSTA error: %s", data.Message)
					break readingLoop
				case *LoginResponce: // ответ на авторизационный запрос
					if data.Code != 0 {
						// формируем ошибку об авторизации
						connErr = fmt.Errorf("login error: [%d] %s", data.Code, data.Message)
						break readingLoop
					}
				}
				// отправляем информацию о всех разобранных событиях для дальнейшей обработки
				if events != nil {
					events <- eventPointer
				}
			}
		}
		// чтение ответов от сервера закончилось
		if keepAliveTicker != nil {
			keepAliveTicker.Stop() // останавливаем таймер для посылки keep-alive сообщений
		}
		if events != nil { // если определен обработчик событий, то...
			if connErr != nil {
				events <- connErr // если была ошибка, то отдаем ее в канал событий
			}
			close(events) // закрываем канал с событиями
		}
	}()

	// отправляем команду на авторизацию, если информация о логине определена
	if c.login != nil {
		if err = c.Login(c.login); err != nil {
			return
		}
	}
	// соединение установлено и инициализировано
	return
}
