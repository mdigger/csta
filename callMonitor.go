package csta

import (
	"fmt"
	"log"
	"reflect"
	"time"
)

// CallMonitor описывает соединение с MX-сервером, которое поддерживает звонки с возможностью их
// мониторинга.
type CallMonitor struct {
	conn     *Conn         // соединение с MX-сервером
	TimeOut  time.Duration // время ожидания ответа на звонок
	monitors MonitorList   // список мониториемых звонков
}

// NewCallMonitor возвращает новый монитор для звонков.
func NewCallMonitor(conn *Conn) *CallMonitor {
	// регистрируем дополнительные события, которые мы хотим обрабатывать
	conn.Register(EventsDataMap{
		"MakeCallResponse":       reflect.TypeOf(MakeCallResponse{}),
		"EstablishedEvent":       reflect.TypeOf(EstablishedEvent{}),
		"OriginatedEvent":        reflect.TypeOf(OriginatedEvent{}),
		"DeliveredEvent":         reflect.TypeOf(DeliveredEvent{}),
		"DivertedEvent":          reflect.TypeOf(DivertedEvent{}),
		"ConnectionClearedEvent": reflect.TypeOf(ConnectionClearedEvent{}),
	})
	// инициализируем список мониторов
	monitors := make(map[string]*Monitor, DefaultMonitorsLength)
	return &CallMonitor{conn: conn, monitors: MonitorList{list: monitors}}
}

// Connect осуществляет подключение к серверу MX.
func (cm *CallMonitor) Connect() error {
	events := make(chan interface{}, 100)           // инициализируем канал для получения событий
	if err := cm.conn.Connect(events); err != nil { // подключаемся к серверу
		return err
	}
	// запускаем поток для обработки всех входящий событий
	go func() {
	eventsLoop: // обрабатываем все получаемые сообщения
		for event := range events {
			// определяем тип данных и события
			switch data := event.(type) {
			case *MakeCallResponse: // начало совершения звонка
				log.Println("MakeCallResponse")
				if !cm.monitors.set(data.DeviceID, data.CallID, CallEventMake) {
					continue
				}
				// запускаем мониторинг звонка
				if err := cm.conn.MonitorStart(data.DeviceID); err != nil {
					break eventsLoop // прерываем всю обработку
				}
				// если установлен таймаут на установку звонка, то запускаем обработчик,
				// который прервет этот процесс по достижении заданного времени
				if cm.TimeOut > 0 {
					// TODO: add timer function
					time.AfterFunc(cm.TimeOut, func() {})
				}
			case *OriginatedEvent: // изменение процесса звонка
				log.Println("OriginatedEvent")
				cm.monitors.set(data.DeviceID, data.CallID, CallEventNone)
			case *DeliveredEvent: // звонок успешно установлен
				log.Println("DeliveredEvent")
				cm.monitors.set(data.DeviceID, data.CallID, CallEventDelivered)
			case *DivertedEvent: // звонок перенаправлен
				log.Println("DivertedEvent")
				cm.monitors.set(data.DeviceID, data.CallID, CallEventDiverted)
			case *EstablishedEvent: // связь с абонентом установлена
				log.Println("EstablishedEvent")
				if !cm.monitors.set("", data.CallID, CallEventEstablished) {
					continue
				}
				// останавливаем мониторинг звонка
				if err := cm.conn.MonitorStop(data.RefID); err != nil {
					break eventsLoop // прерываем всю обработку
				}
			case *ConnectionClearedEvent: // процесс звонка завершен не удачно
				log.Println("ConnectionClearedEvent")
				if !cm.monitors.set(data.DeviceID, data.CallID, CallEventClear) {
					continue
				}
				// останавливаем мониторинг звонка
				if err := cm.conn.MonitorStop(data.RefID); err != nil {
					break eventsLoop // прерываем всю обработку
				}
			case error: // ошибка соединения с сервером
				log.Println("error:", data)
				break eventsLoop // прерываем всю обработку
			}
		}
		// соединение с сервером закрыто и события больше не поступают
		cm.monitors.connectionError() // уведомить всех оставшихся об ошибке
		cm.conn.Close()               // закрываем соединение
	}()
	return nil
}

// MakeCall инициирует дозвон сервером и установление голосового соединения между двумя абонентами
// с указанными телефонными номерами.
func (cm *CallMonitor) MakeCall(ext, to string) (<-chan CallEvent, error) {
	events := cm.monitors.add(ext, to) // добавляем в список звонков и получаем канал
	if events == nil {
		return nil, fmt.Errorf("callMonitor: device %q already in call", ext)
	}
	// отсылаем запрос на установку звонка серверу
	if err := cm.conn.MakeCall(ext, to); err != nil { // инициируем звонок от сервера
		return nil, err
	}
	return events, nil // возвращаем канал со статусами звонка
}
