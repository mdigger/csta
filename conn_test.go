package csta

import (
	"fmt"
	"log"
	"testing"
	"time"
)

var (
	address = &Address{
		Server:               "voip.xyzrd.com:7778", //"10.0.1.204:7778",
		IsTLS:                true,
		IsInsecureSkipVerify: true,
	}
	login = &Login{
		Name:     "d3",
		Password: "9185",
	}
)

func TestConnAddressbook(t *testing.T) {
	conn := NewServerAddressbook(NewConn(address, login), nil, nil)
	events := make(chan interface{}, 100)
	if err := conn.Connect(events); err != nil {
		t.Fatal(err)
	}
	time.Sleep(time.Second * 10)
	for ext, user := range conn.AddressBook.list {
		fmt.Println(ext, user.FirstName, user.LastName)
	}
	for event := range events {
		log.Printf("%#v", event)
	}
}
