package csta

import (
	"errors"
	"fmt"
	"text/template"
)

// Login отсылает информацию для авторизации. В качестве параметров передается информация о логине
// и флаг, указывающий что пароль необходимо передавать в открытом виде.
func (c *Conn) Login(login *Login) error {
	if login == nil {
		return errors.New("no login info")
	}
	var password string
	if login.AsClear {
		password = login.Password
	} else {
		password = login.hashPassword()
	}
	return c.Send(fmt.Sprintf(`<loginRequest type="%s" platform="%s" version="%s">`+
		`<userName>%s</userName><pwd>%s</pwd></loginRequest>`,
		template.HTMLEscapeString(login.getType()),
		template.HTMLEscapeString(login.getPlatform()),
		template.HTMLEscapeString(login.getVersion()),
		template.HTMLEscapeString(login.Name),
		password))
}

// MakeCall отсылает команду инициализации звонка.
func (c *Conn) MakeCall(from, to string) error {
	return c.Send(fmt.Sprintf(`<MakeCall><callingDevice typeOfNumber="deviceID">%s</callingDevice>`+
		`<calledDirectoryNumber>%s</calledDirectoryNumber></MakeCall>`,
		template.HTMLEscapeString(from),
		template.HTMLEscapeString(to)))
}

// ClearConnection отсылает команду для прерывания звонка. В качестве параметров передается
// внутренний номер абонента, сделавшего звонок, и идентификатор этого звонка, полученный ранее.
func (c *Conn) ClearConnection(from, callID string) error {
	return c.Send(fmt.Sprintf(`<ClearConnection xmlns="http://www.ecma.ch/standards/ecma-323/csta/ed2">`+
		`<connectionToBeCleared><callID>%s</callID><deviceID>%s</deviceID></connectionToBeCleared></ClearConnection>`,
		template.HTMLEscapeString(callID),
		template.HTMLEscapeString(from)))
}

// MonitorStart отсылает команду для начала мониторинга всех событий на сервере для указанного
// номера телефона (внутреннего номера).
func (c *Conn) MonitorStart(ext string) error {
	return c.Send(fmt.Sprintf(`<MonitorStart xmlns="http://www.ecma-international.org/standards/ecma-323/csta/ed4">`+
		`<monitorObject><deviceObject>%s</deviceObject></monitorObject></MonitorStart>`,
		template.HTMLEscapeString(ext)))
}

// MonitorStop отправляет команду для остановки мониторинга событий на сервере. В качестве параметра
// передается идентификатор монитора, запущенный ранее.
func (c *Conn) MonitorStop(refID int) error {
	return c.Send(fmt.Sprintf(`<MonitorStop xmlns="http://www.ecma-international.org/standards/ecma-323/csta/ed4">`+
		`<monitorCrossRefID>%d</monitorCrossRefID></MonitorStop>`,
		refID))
}

// GetAddressBook запрашивает получение адресной книги. В качестве параметра указывается индекс
// необходимого блока с информацией по контактам пользователей на сервере.
func (c *Conn) GetAddressBook(index int) error {
	return c.Send(fmt.Sprintf(`<iq type="get" id="addressbook" index="%d" />`, index))
}

// GetBuddyList запрашивает получение списка избранных пользователей.
func (c *Conn) GetBuddyList() error {
	return c.Send(`<iq type="get" id="roster" />`)
}

// AddToBuddyList добавляет пользователя в список избранных.
func (c *Conn) AddToBuddyList(JID string) error {
	return c.Send(fmt.Sprintf(`<iq type="set" id="buddy" jid="%s" />`,
		template.HTMLEscapeString(JID)))
}

// RemoveFromBuddyList удаляет пользователя из списка избранных.
func (c *Conn) RemoveFromBuddyList(JID string) error {
	return c.Send(fmt.Sprintf(`<iq type="remove" id="buddy" jid="%s" />`,
		template.HTMLEscapeString(JID)))
}
