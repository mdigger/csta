package csta

import (
	"reflect"
	"sync"
)

// EventsDataMap описывает ассоциацию поддерживаемых событий.
type EventsDataMap map[string]reflect.Type

// Events описывает список поддерживаемых событий, возвращаемых сервером, и ассоциированные
// с ними структуры данных. Тип должен быть типом структуры, а не указателем на него,
type Events struct {
	list EventsDataMap // список ассоциаций поддерживаемых событий и структур для их разбора
	mu   sync.RWMutex
}

// getEventData возвращает указатель на новое значение структуры для разбора данных события.
func (e *Events) getEventData(event string) interface{} {
	e.mu.RLock()
	dataType, ok := e.list[event]
	e.mu.RUnlock()
	if ok && dataType != nil {
		return reflect.New(dataType).Interface()
	}
	return nil
}

// Register регистрирует обрабатываемые события и ассоциирует их со структурой данных.
func (e *Events) Register(events EventsDataMap) {
	e.mu.Lock()
	if e.list == nil {
		e.list = make(map[string]reflect.Type, len(events))
	}
	for event, eventType := range events {
		if eventType == nil {
			delete(e.list, event)
		} else {
			e.list[event] = eventType
		}
	}
	e.mu.Unlock()
}

// DefaultEvents является списком поддерживаемых ответов сервера и ассоциированные с ним структуры
// данных. Вы можете добавить или переопределить описание структур данных, которые будут
// ассоциированы с каждым из событиев. Если по каким-то причинам вы не хотите обрабатываеть
// некоторые события и разбирать данные с ними, вы можете просто удвалить их из списка или
// установить их значение в nil.
var DefaultEvents = EventsDataMap{
	"CSTAErrorCode":          reflect.TypeOf(ErrorCode{}),
	"loginResponce":          reflect.TypeOf(LoginResponce{}),
	"loginFailed":            reflect.TypeOf(LoginResponce{}),
	"ablist":                 reflect.TypeOf(ABList{}),
	"contact":                reflect.TypeOf(Contact{}),
	"removeContact":          reflect.TypeOf(RemoveContact{}),
	"presence":               reflect.TypeOf(Presence{}),
	"MakeCallResponse":       reflect.TypeOf(MakeCallResponse{}),
	"ConnectionClearedEvent": reflect.TypeOf(ConnectionClearedEvent{}),
	"MonitorStartResponse":   reflect.TypeOf(MonitorStartResponse{}),
	"MonitorStopResponse":    reflect.TypeOf(MonitorStopResponse{}),
	"EstablishedEvent":       reflect.TypeOf(EstablishedEvent{}),
	"OriginatedEvent":        reflect.TypeOf(OriginatedEvent{}),
	"DeliveredEvent":         reflect.TypeOf(DeliveredEvent{}),
	"DivertedEvent":          reflect.TypeOf(DivertedEvent{}),
}

// LoginResponce описывает ответ на логин (события loginResponce или loginFailed)
type LoginResponce struct {
	Code       int    `xml:"Code,attr"`       // код ошибки
	SN         string `xml:"sn,attr"`         // серийный номер
	APIVersion int    `xml:"apiversion,attr"` // версия API
	Ext        string `xml:"ext,attr"`        // внутренний телефонный номер пользователя
	JID        string `xml:"userId,attr"`     // внутренний уникальный идентификатор пользователя
	Message    string `xml:",chardata"`       // текст сообщения
}

// === Пользовательские данные

// ABEntry описывает информацию об одной записи в адресной книге (используется в ablist).
type ABEntry struct {
	JID        string `xml:"jid,attr"`      // внутренний уникальный идентификатор пользователя
	Ext        string `xml:"businessPhone"` // внутренний телефонный номер
	FirstName  string `xml:"firstName"`     // имя пользователя
	LastName   string `xml:"lastName"`      // фамилия
	HomePhone  string `xml:"homePhone"`     // домашний телефон
	CellPhone  string `xml:"cellPhone"`     // сотовый телефон
	Email      string `xml:"email"`         // email адрес
	ExchangeID string `xml:"exchangeId"`    // уникальный идентификатор пользователя в MS Exchane
}

// ABList описывает информацию о полученном списке пользователей в адресной книге (событие ablist).
type ABList struct {
	Size      int       `xml:"size,attr"`  // общий размер адресной книги на сервере
	Index     int       `xml:"index,attr"` // индекс первого элемента в этом списке относительно адресной книги
	ABEntries []ABEntry `xml:"abentry"`    // порция данных о пользователях из адресной книги
}

// Contact описывает информацию об избранном контакте (событие contact).
type Contact struct {
	JID    string `xml:"jid,attr"`      // внутренний уникальный идентификатор пользователя
	Ext    string `xml:"phone,attr"`    // внутренний телефонный номер
	Name   string `xml:"name,attr"`     // отображаемое имя пользователя
	Status string `xml:"presence,attr"` // статус
	Note   string `xml:"presenceNote"`  // заметка пользователя о статусе
}

// RemoveContact описывает информацию об удалении контакта из избранных.
type RemoveContact struct {
	JID string `xml:"jid,attr"` // внутренний уникальный идентификатор пользователя
}

// Presence описывает информацию об измененном статусе избранного пользователя (событие presence).
type Presence struct {
	JID    string `xml:"from,attr"`    // внутренний уникальный идентификатор пользователя
	Status string `xml:"status,attr"`  // статус пользователя
	Note   string `xml:"presenceNote"` // заметка пользователя о статусе
}

// === Системные события

// MakeCallResponse описывает информацию о начале звонка.
type MakeCallResponse struct {
	CallID   string `xml:"callingDevice>callID"`
	DeviceID string `xml:"callingDevice>deviceID"`
}

// MonitorStartResponse описывает информацию о начале мониторинга.
type MonitorStartResponse struct {
	RefID int `xml:"monitorCrossRefID"`
}

// MonitorStopResponse описывает информацию об окончании мониторинга.
type MonitorStopResponse MonitorStartResponse

// ConnectionClearedEvent описывает информацию об очистке звонка.
type ConnectionClearedEvent struct {
	RefID    int    `xml:"monitorCrossRefID"`
	CallID   string `xml:"droppedConnection>callID"`
	DeviceID string `xml:"droppedConnection>deviceID"`
}

// EstablishedEvent описывает информацию об установленном звонке.
type EstablishedEvent struct {
	RefID  int    `xml:"monitorCrossRefID"`
	CallID string `xml:"establishedConnection>callID"`
	From   string `xml:"callingDevice>deviceIdentifier"`
	To     string `xml:"calledDevice>deviceIdentifier"`
}

// OriginatedEvent описывает информацию о начале действия.
type OriginatedEvent struct {
	RefID    int    `xml:"monitorCrossRefID"`
	CallID   string `xml:"originatedConnection>callID"`
	DeviceID string `xml:"originatedConnection>deviceID"`
}

// DeliveredEvent описывает информацию о выполнении.
type DeliveredEvent struct {
	RefID    int    `xml:"monitorCrossRefID"`
	CallID   string `xml:"connection>callID"`
	DeviceID string `xml:"connection>deviceID"`
}

// DivertedEvent описывает информацию об отклонении.
type DivertedEvent DeliveredEvent

// ErrorCode (CSTAErrorCode) описывает информацию об CSTA-ошибке.
type ErrorCode struct {
	Message string `xml:",any"`
}
